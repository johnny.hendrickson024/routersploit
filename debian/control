Source: routersploit
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12), dh-python, python3-all, python3-setuptools
Standards-Version: 4.5.1
Homepage: https://github.com/threat9/routersploit
Vcs-Git: https://gitlab.com/kalilinux/packages/routersploit.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/routersploit

Package: routersploit
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Recommends: python3-bluepy
Description: Exploitation Framework for Embedded Devices
 This package contains an open-source exploitation framework dedicated to
 embedded devices. It consists of various modules that aids penetration testing
 operations:
  * exploits - modules that take advantage of identified vulnerabilities.
  * creds - modules designed to test credentials against network services.
  * scanners - modules that check if target is vulnerable to any exploit.
  * payloads - modules that are responsible for generating payloads for various
    architectures and injection points.
  * generic - modules that perform generic attacks.
